/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import RedPetri.Monitores;

/**
 *
 * @author patop
 */
public class Hilos implements Runnable{

    private int[][] disparo;
    private String sNombre;

    public Hilos(int[][] disparo, String sNombre) {
        this.disparo = disparo;
        this.sNombre = sNombre;
    }
    
    @Override
    public void run() {
        Monitores.instance(null, null).ejecutar(sNombre, disparo);
    }
    
}
