/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import clases.LeerArchivo;
import RedPetri.Monitores;
import RedPetri.Politicas;
import RedPetri.Redes;
import clases.Hilos;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Patricio.
 * This file is not really a main class, it is just a simple system test.
 * It proves that the html and the xls file are read,  the monitor works 
 * with petri nets and only one threads is inside the monitor.
 */
public class Main {
    static Redes oRed;
    static Monitores oMonitor;
    public static void main(String[] args) {          
    
        int[][] iDisparo0  = {{1, 0, 0, 0, 0}};
        int[][] iDisparo1  = {{0, 1, 0, 0, 0}};
        int[][] iDisparo2  = {{0, 0, 1, 0, 0}};
        int[][] iDisparo3  = {{0, 0, 0, 1, 0}};
        int[][] iDisparo4  = {{0, 0, 0, 0, 1}};
        LeerArchivo leerArchivo = new LeerArchivo();
        HashMap<String,int[][]> datos = leerArchivo.LeerHTML();
        oRed = new Redes(datos.get("marcado"), datos.get("incidencia"),datos.get("inhibicion"),datos.get("automaticas"));
        Politicas oPolitica = Politicas.Instance(datos.get("prioridad"));
        oMonitor = Monitores.instance(oRed,oPolitica);
        
        Thread oThread0 = new Thread(new Hilos(iDisparo0, "Prueba0"));
        Thread oThread = new Thread(new Hilos(iDisparo1, "Prueba1"));
        Thread oThread2 = new Thread(new Hilos(iDisparo2, "Prueba2"));
        Thread oThread3 = new Thread(new Hilos(iDisparo3, "Prueba3"));
        Thread oThread4 = new Thread(new Hilos(iDisparo4, "Prueba4"));
        
        oThread.start();
        oThread0.start();
        oThread2.start();
        oThread3.start();
        oThread4.start();
    }
    
}
