/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedPetri;

import clases.Matriz;

/**
 * This class is used for prioritise one shoot from another.
 * Rows indicates the priority of the transition and columns simbolized the 
 * number the transition. It is also a Singleton class. TransicionAEjecutar is
 * is the method which receives the sensibilized transitions and returns the 
 * priority shoot.
 */
public class Politicas {
    
    private static Politicas oPolitica = null;
    private boolean print = true;
    private int[][] iPrioridad = null;
    
    private Politicas()
    {
    }
    
    private Politicas(int[][] iPolitica){
        this.iPrioridad = iPolitica;
    }

    public static Politicas Instance(int[][] iPolitica){
        if(oPolitica == null)
            oPolitica = new Politicas(iPolitica);
        return oPolitica;
    }

    public int[][] getiPolitica() {
        return iPrioridad;
    }

    public void setiPolitica(int[][] iPolitica) {
        this.iPrioridad = iPolitica;
    }

    public int[][] TransicionAEjecutar(Matriz oSensibilizadas){
        Matriz oPrioridad = new Matriz(iPrioridad);
        Matriz oEjecutar = oSensibilizadas.mult(oPrioridad);
        Matriz oDisparar = new Matriz(1, oEjecutar.getColCount());
        for (int i = 0; i < oDisparar.getColCount(); i++) {
            oDisparar.setDato(0, i, oEjecutar.getVal(0, i));
            for (int j = 0; j < i; j++) {
                if(oEjecutar.getVal(0, j) == 0)
                    oDisparar.setDato(0, i, oDisparar.getVal(0, i)*1);
                else
                    oDisparar.setDato(0, i, oDisparar.getVal(0, i)*0);
            }
        }
        Matriz ms = oPrioridad.mult(oDisparar.transpose());
        return ms.transpose().getDato();
    }
    
}
