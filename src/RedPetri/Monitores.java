/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedPetri;

import clases.Matriz;
import clases.Ficheros;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * I choosed this code because is different from the others projects I worked. 
 * This has project has concurrency and threads. For my it was a chanllege since
 * I have always worked in single-threads projects. It showed me that using UML 
 * diagrams is worth it beacause they save a lot of time, because they let you
 * see where you are, where are you going to go and how are you going to finish
 * it.
 * 
 * This class is a singleton class. It online has 1 publc method It 
 * is used by Threads in order to access a critical section. To get access to 
 * the critical section, the thread's shoot must be succefully executed. If it 
 * is not, it will be sent to sleep until another thread wakes him when his 
 * shoot can be executed.
 * After getting access to the critical section, it will check if there are 
 * threads waiting for being waked. If there is more than one, it will executed
 * a policy matrix (which has the priority between the threads) which return
 * which of the waiting threads will be executed.
 * This monitor also supports soft timetable petri nets but it can be changed to
 * support hard timetable petri nets.
 * There is a boolean called "print" which enables the monitor to print 
 * messages while each step is taken.
 */
public class Monitores {

    private static Monitores oMonitor = null;

    private Monitores() {
        this.oEntrada = new Semaphore(1,true);
        this.print = true;
        this.CantTransiciones = new ArrayList<>();
        this.transiciones = new ArrayList<>();
        this.lock = new ReentrantLock();
    }
    private ReentrantLock lock;
    private ArrayList<Condition> transiciones;
    private ArrayList<Integer> CantTransiciones;
    private Redes oRed;
    private Politicas oPolitica;
    private boolean print;
    private Semaphore oEntrada;
    
    private Monitores(Redes oRed, Politicas oPolitica) {
        this.oEntrada = new Semaphore(1,true);
        this.print = true;
        this.CantTransiciones = new ArrayList<>();
        this.transiciones = new ArrayList<>();
        this.lock = new ReentrantLock();
        this.oRed = oRed;
        this.oPolitica = oPolitica;
//        oEntrada = lock.newCondition();
        for (int i = 0; i < oRed.getCantidadTransiciones(); i++) {
            transiciones.add(lock.newCondition());
            CantTransiciones.add(0);
        }
    }

    public static Monitores instance(Redes oRed, Politicas oPolitica) {
        if (oMonitor == null) {
            oMonitor = new Monitores(oRed, oPolitica);
        }
        return oMonitor;
    }
    
    public void ejecutar(String sNombre, int[][] disparo) {
        try {
            Ficheros.Instance().Escribir("MONITOR","----" + sNombre + " quiere el semaforo");
            oEntrada.acquire();
            if(print)
                Ficheros.Instance().Escribir("MONITOR","----" + sNombre + "pasa el semaforo");
        } catch (InterruptedException ex) {
            Logger.getLogger(Monitores.class.getName()).log(Level.SEVERE, null, ex);
        }
        lock.lock();
        try {
            if (print) {
                Ficheros.Instance().Escribir("MONITOR","----Ingresa " + sNombre + " al monitor");
            }
            ArrayList<Integer> transNoDisparadas = oRed.ejecutar(disparo);
            while (transNoDisparadas != null)//disparo para poder cEntrada.
            {
                int trans = 0;
                if (transNoDisparadas.size() != 1) {
                    trans = obtenerTransPorPolitica(transNoDisparadas);
                } else {
                    trans = transNoDisparadas.get(0);
                }
                if (print) {
                    Ficheros.Instance().Escribir("MONITOR","---" + sNombre + " no se ejecuta y se almacena en la cola N°" + trans);
                }
                try {
                    CantTransiciones.set(trans, CantTransiciones.get(trans) + 1);
                    oEntrada.release();
                    transiciones.get(trans).await();
                } catch (InterruptedException ex) {
                    Ficheros.Instance().Escribir("MONITOR",ex.getMessage());
                }
                transNoDisparadas = oRed.ejecutar(disparo);
            }
            if (print) {
                Ficheros.Instance().Escribir("MONITOR","---" + sNombre + " se ejecuta");
            }
            Matriz oEsperando = enEspera();
            if (oEsperando != null) {
                Matriz oSensibilizadas = oRed.getSensibilizadasTemporales();
                if (print) {
                    Ficheros.Instance().Escribir("MONITOR","--" + "Sensibilizadas: {" + oSensibilizadas.toString() + "}");
                }
                Matriz AND = oEsperando.AND(oSensibilizadas);
                if (print) {
                    Ficheros.Instance().Escribir("MONITOR","--" + "AND: {" + AND.toString() + "}");
                }
                int cantActive = Matriz.TransActive(AND.getDato());
                if (print) {
                    Ficheros.Instance().Escribir("MONITOR","--" + "Transiciones Posibles a Disparar: " + cantActive);
                }
                if (cantActive > 0) {
                    if (cantActive == 1) {// Si solo hay 1 transición activa
                        int t = obtenerTransActiva(AND.getDato());
                        if (print) {
                            Ficheros.Instance().Escribir("MONITOR","--" + "Se libera el disparo " + t);
                        }
                        CantTransiciones.set(t, CantTransiciones.get(t) - 1);
                        transiciones.get(t).signal();
                    } else {//Si hay mas de una, se lla a políticas
                        int[][] newDisparo = oPolitica.TransicionAEjecutar(AND);
                        Matriz newMatriz = new Matriz(newDisparo);
                        if (print) {
                            Ficheros.Instance().Escribir("MONITOR","--" + "Disparo de Politica: {" + newMatriz.toString() + "}");
                        }
                        int t = obtenerTransActiva(newDisparo);

                        Ficheros.Instance().Escribir("MONITOR","--" + "Se libera el disparo " + t);
                        CantTransiciones.set(t, CantTransiciones.get(t) - 1);
                        transiciones.get(t).signal();
                    }
                }
            }
        } finally {
            if (print) {
                Ficheros.Instance().Escribir("MONITOR","----" + sNombre + " sale del monitor");
            }
            lock.unlock();
            if(!lock.hasQueuedThreads())
                oEntrada.release();
        }
    }

    private Matriz enEspera() {
        ArrayList<Integer> esperando = new ArrayList<>();
        for (int i = 0; i < CantTransiciones.size(); i++) {
            if (CantTransiciones.get(i) != 0) {
                esperando.add(i);
//                if (print) {
//                    Ficheros.Instance().Escribir("MONITOR","Cola Nª" + i + " tiene " + CantTransiciones.get(i) + " en la cola.");
//                }
            }
        }
        if (!esperando.isEmpty()) {
            int[][] iEsperando = new int[1][transiciones.size()];
            for (int i = 0; i < esperando.size(); i++) {
                iEsperando[0][esperando.get(i)] = 1;
            }
            Matriz oEsperando = new Matriz(iEsperando);
            if (print) {
                Ficheros.Instance().Escribir("MONITOR","--" + "Esperando: {" + oEsperando.toString() + "}");
            }
            return oEsperando;
        }
        if (print) {
            Ficheros.Instance().Escribir("MONITOR","--" + "No existen transiciones en espera");
        }
        return null;
    }

    private int obtenerTransPorPolitica(ArrayList<Integer> transNoDisparadas) {
        int trans = 0;
        int[][] disparo = new int[1][CantTransiciones.size()];
        for (int i = 0; i < disparo.length; i++) {
            for (int j = 0; j < CantTransiciones.size(); j++) {
                disparo[i][j] = 0;
            }
        }
        for (int i = 0; i < transNoDisparadas.size(); i++) {
            disparo[0][transNoDisparadas.get(i)] = 1;
        }
        return obtenerTransActiva(oPolitica.TransicionAEjecutar(new Matriz(disparo)));
    }

    private int obtenerTransActiva(int[][] resultado) {
        int t = 0;
        for (int i = 0; i < resultado.length; i++) {
            for (int j = 0; j < resultado[i].length; j++) {
                if (resultado[i][j] == 1) {
                    t = j;
                }
            }
        }
        return t;
    }
}
