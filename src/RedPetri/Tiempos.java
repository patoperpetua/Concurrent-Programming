/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedPetri;

import java.util.Date;

/**
 * This class is used to see if a transition is sensibilized by time. It simple
 * checks if the amount of time passed from the beginning of the sensiblization
 * of the transition. It was made to use with soft timed transitions but it can
 * be easily changed for the hard kind.
 */
public class Tiempos {
    
    private Date oInicio;
    private int iInicio;
    private int iFinal;
    
    public Tiempos(boolean bActivate,int iInicio, int iFinal){
        this.iFinal = iFinal;
        this.iInicio = iInicio;
        if(bActivate)
            oInicio = new Date();
    }
    
    public boolean isActive(){
        return oInicio != null;
    }
    
    public boolean isInTime(){
        if(isActive()){
           if(new Date().getTime() - oInicio.getTime() > iInicio){
//               if(oInicio.before(new Date(oInicio.getTime()+iFinal)))
                   return true;
//               else
//                   return false;
           }
           else
               return false;
        }
        return isActive();
    }
    
    public void starts(){
        if(oInicio == null)
            oInicio = new Date();
        else
            System.out.println("TIEMPO YA INICIADO ANTERIORMENTE");
    }
    
    public void reset(){
        oInicio = null;
    }

    public int getiInicio() {
        return iInicio;
    }

    public int getiFinal() {
        return iFinal;
    }
}
